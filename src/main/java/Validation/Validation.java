package Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

	private static Pattern pattern;
	private static Matcher matcher;

	private static final String NAME_PATTERN = "^[A-Za-z]{3,15}$";
	private static final String SURNAME_PATTERN = "^[A-Za-z]{3,15}$";
	private static final String PASSWORD_PATTERN = "^[!@#$%&*+=:;,.?<>A-Za-z0-9_-]{6,15}$";
	private static final String EMIAL_PATTERN = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.(?:[A-Z]|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)$";
	
	public static boolean PasswordValidator(final String text) {
		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(text);
		return matcher.matches();
	}
	
	public static boolean NameValidator(final String text) {
		pattern = Pattern.compile(NAME_PATTERN);
		matcher = pattern.matcher(text);
		return matcher.matches();
	}
	public static boolean SurnameValidator(final String text) {
		pattern = Pattern.compile(NAME_PATTERN);
		matcher = pattern.matcher(text);
		return matcher.matches();
	}
	public static boolean EmailValidator(final String text) {
		pattern = Pattern.compile(EMIAL_PATTERN);
		matcher = pattern.matcher(text);
		return matcher.matches();
	}
	
}
