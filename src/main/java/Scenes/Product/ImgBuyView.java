package Scenes.Product;

import java.util.ArrayList;

import Database.MainDatabase;
import Database.entities.OrderDetail;
import Database.entities.Product;
import Database.entities.Transaction;
import Database.entities.User;
import Scenes.ShopScene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class ImgBuyView extends HBox {
	
	private Button imgButton;
	private Button addToBucketButton;
	private Button viewButton;
	
	private AddProductWindow addProductWindow;
	private ShopScene shopScene;
	
	private Product product;
	private User user;
	private Transaction transaction;
	private OrderDetail orderDetail;
	
	public ImgBuyView(AddProductWindow addProductWindow,ShopScene shopScene,User user) {
		this.addProductWindow = addProductWindow;
		this.user = user;
		
		initButtons(addProductWindow, shopScene);
		
		this.setSpacing(4);
		this.getChildren().addAll(imgButton,addToBucketButton,viewButton);
	}

	private void initButtons(AddProductWindow addProductWindow,ShopScene shopScene) {
		imgButton = new Button("IMG");
		
		addToBucketButton = new Button("ADD TO BUCKET");
		addToBucketButton.setOnAction(e ->
		{
			createTransaction(addProductWindow);
			
		//	MainDatabase.remove(product);
		//	MapOfProducts.remove(product);
			
			shopScene.getLayoutProducts().getChildren().remove(this);
		});
		
		viewButton = new Button("VIEW");
	}

	private void createTransaction(AddProductWindow addProductWindow) {
		product = addProductWindow.getProduct();
		
		transaction = new Transaction();
		transaction.setTest("test");
		transaction.setOrderDetails(new ArrayList<>());
		user.getTransactions().add(transaction);
		MainDatabase.add(transaction);
		
		orderDetail = new OrderDetail();
		
		product.getOrderDetails().add(orderDetail);
		transaction.getOrderDetails().add(orderDetail);
		
		MainDatabase.add(orderDetail);
		
	}
	
	/*
	 * GETTERS AND SETTERS
	 * 
	 * 
	 */

}
