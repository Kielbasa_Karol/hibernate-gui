package Scenes.Product;

import java.util.HashMap;
import java.util.Map;

import Database.entities.Product;

public class MapOfProducts {
	
	  static Map<Product, Integer> map = new HashMap<Product, Integer>();
	  
	  public static void add(Product product)
	  {
		  map.put(product, (int) product.getId());
	  }
	  
	  public static void remove(Product product)
	  {
		  map.remove(product);
	  }

}
