package Scenes.Product;

import java.util.ArrayList;

import Database.MainDatabase;
import Database.entities.Product;
import Scenes.ShopScene;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AddProductWindow {

	private Stage window;
	private Label nameLabel;
	private Label priceLabel;
	private Button addProductButton;
	private TextField nameTextField;
	private TextField priceTextField;
	private GridPane layout;
	private Scene scene;

	private Product product;
	private ShopScene shopScene;

	public AddProductWindow(ShopScene shopScene) {
		this.shopScene = shopScene;
	}

	public void showAddProductWindow() {
		window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle("ADD PRODUCT");
		window.setResizable(false);

		initLabels();
		initButton();
		initTextFields();
		initLayout();
		initScene();
	}

	private void initScene() {
		scene = new Scene(layout, 210, 130);
		window.setScene(scene);
		window.show();
	}

	private void initLayout() {
		layout = new GridPane();
		layout.setPadding(new Insets(10, 10, 10, 10));
		layout.setVgap(15);
		layout.setHgap(10);
		layout.setStyle("-fx-background-color: DAE6F3;");
		layout.getChildren().addAll(nameLabel, nameTextField, priceLabel, priceTextField, addProductButton);
	}

	private void initTextFields() {
		nameTextField = new TextField();
		GridPane.setConstraints(nameTextField, 1, 0);

		priceTextField = new TextField();
		GridPane.setConstraints(priceTextField, 1, 1);
	}

	private void initButton() {
		addProductButton = new Button("ADD");
		addProductButton.setTranslateX(80);
		addProductButton.setTranslateY(85);
		addProductButton.setOnAction(e -> {
			loadData();
			window.close();
		});
	}

	private void loadData() {
		product = new Product();
		product.setOrderDetails(new ArrayList<>());
		
		MapOfProducts.add(product);

		product.setName(nameTextField.getText());

		String priceText = priceTextField.getText();
		double price = Double.parseDouble(priceText);
		product.setPrice(price);

		MainDatabase.add(product);
	}

	private void initLabels() {
		nameLabel = new Label("name: ");
		GridPane.setConstraints(nameLabel, 0, 0);

		priceLabel = new Label("price: ");
		GridPane.setConstraints(priceLabel, 0, 1);
	}

	/*
	 * 
	 * GETTERS AND SETTERS
	 * 
	 */
	public Product getProduct() {
		return product;
	}
}
