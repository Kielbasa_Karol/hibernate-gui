package Scenes;

import Main.Main;
import javafx.scene.Scene;
import javafx.stage.Stage;

public abstract class AbstractScene {
	
	protected Stage window;
	protected Scene scene;
	protected static Main main;
	
	public AbstractScene(Stage window,Main main,double width,double height)
	{
		this.window = window;
		this.main = main;
	}
	
	protected abstract void initScene(double width, double height);
	protected abstract void initLayout();
	protected abstract void initButtons();
	protected abstract void initLabels();

}
