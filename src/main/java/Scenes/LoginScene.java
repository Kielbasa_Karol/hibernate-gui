package Scenes;

import Database.MainDatabase;
import Main.Main;
import Window.AlertWindow;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class LoginScene extends AbstractScene {
	
	private GridPane layout;
	private Label emailLabel;
	private Label passwordLabel;
	private Label titleLabel;
	private TextField emailTextField;
	private PasswordField passPasswordField;
	private Button loginButton;
	private Button backButton;
	
	public LoginScene(Stage window,Main main, double width,double height) {
		super(window,main ,height, height);
		
		init(width, height);
		
		window.setScene(scene);
	}

	private void init(double width, double height) {
		initButtons();
		initPasswordField();
		initTextField();
		initLabels();
		initLayout();
		initScene(width,height);
	}

	private void initPasswordField() {
		passPasswordField = new PasswordField();
		passPasswordField.setText("password");
		GridPane.setConstraints(passPasswordField, 1, 1);
	}

	private void initTextField() {
		emailTextField = new TextField("example@ex.com");
		GridPane.setConstraints(emailTextField, 1, 0);
	}

	@Override
	protected void initScene(double width, double height) {
		scene = new Scene(layout,width,height);
		
	}

	@Override
	protected void initLayout() {
		layout = new GridPane();
		layout.setPadding(new Insets(200,80,100,80));
		layout.setVgap(8);
		layout.setHgap(10);
		layout.setStyle("-fx-background-color: DAE6F3;");
		layout.getChildren().addAll(emailLabel,emailTextField,passwordLabel,
				passPasswordField,titleLabel,loginButton,backButton);
	}

	@Override
	protected void initButtons() {
		loginButton = new Button("   Log in   ");
		GridPane.setConstraints(loginButton, 0, 2);
		loginButton.setTranslateX(90);
		loginButton.setTranslateY(10);
		loginButton.setOnAction(e -> {
			if(CheckIfExist() == true)
				new ShopScene(window,main,MainDatabase.loadUser(passPasswordField.getText(),emailTextField.getText()),
						main.WIDTH,main.HEIGHT);
			else{
				new AlertWindow();
			}
		});
		
		backButton = new Button("back");
		backButton.setTranslateX(190);
		backButton.setTranslateY(150);
		backButton.setOnAction(e -> window.setScene(main.getStartScene().getScene()));
	}

	private boolean CheckIfExist() {
		return MainDatabase.find(passPasswordField.getText(), emailTextField.getText());
	}

	@Override
	protected void initLabels() {
		emailLabel = new Label("email:");
		GridPane.setConstraints(emailLabel, 0, 0);
		
		passwordLabel = new Label("password:");
		GridPane.setConstraints(passwordLabel, 0, 1);
		
		titleLabel = new Label("LOG IN");
		titleLabel.setTranslateY(-100);
		titleLabel.setTranslateX(100);
	}
}
