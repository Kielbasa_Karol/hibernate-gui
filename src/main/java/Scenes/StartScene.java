package Scenes;

import Main.Main;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class StartScene extends AbstractScene {

	private StackPane layout;
	private Button signUpButton;
	private Button registerButton;
	private Label titleLabel;
	private Scene scene;

	public StartScene(Stage window, Main main, double width, double height) {
		super(window, main, width, height);

		initLabels();
		initButtons();
		initLayout();
		initScene(height, height);

		window.setScene(scene);
	}

	@Override
	protected void initLabels() {
		titleLabel = new Label("BAZA DANYCH + GUI");
		titleLabel.setTranslateY(-100);
	}

	@Override
	protected void initScene(double width, double height) {
		scene = new Scene(layout, width, height);
	}

	@Override
	protected void initLayout() {
		layout = new StackPane();
		layout.setStyle("-fx-background-color: DAE6F3;");
		layout.getChildren().addAll(signUpButton, registerButton, titleLabel);
	}

	@Override
	protected void initButtons() {
		signUpButton = new Button("LOG IN");
		signUpButton.setTranslateX(100);
		signUpButton.setTranslateY(50);
		signUpButton.setOnAction(e -> new LoginScene(window, main, main.WIDTH, main.HEIGHT));

		registerButton = new Button("REGISTER");
		registerButton.setTranslateX(-100);
		registerButton.setTranslateY(50);
		registerButton.setOnAction(e -> new RegistrationScene(window, main, main.WIDTH, main.HEIGHT));
	}

	/**
	 * 
	 * GETTERS AND SETTERS
	 * 
	 * 
	 */

	public Scene getScene() {
		return scene;
	}
}
