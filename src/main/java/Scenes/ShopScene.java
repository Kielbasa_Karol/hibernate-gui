package Scenes;

import Database.entities.User;
import Main.Main;
import Scenes.Product.AddProductWindow;
import Scenes.Product.ImgBuyView;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ShopScene extends AbstractScene {

	private VBox layoutProducts;
	private HBox layoutMenu;
	private Button logOutButton;
	private Button addProductButton;
	
	private ImgBuyView product;
	private User user;
	
	public ShopScene(Stage window, Main main,User user, double width, double height) {
		super(window, main, width, height);
		this.user = user;
		
		
		initLabels();
		initButtons();
		initLayout();
		initScene(width, height);

		window.setScene(scene);
	}

	@Override
	protected void initScene(double width, double height) {
		scene = new Scene(layoutMenu, width, height);
	}

	@Override
	protected void initLayout() {
		layoutMenu = new HBox();
		layoutMenu.setPadding(new Insets(15, 12, 15, 12));
		layoutMenu.setSpacing(10);
		layoutMenu.getChildren().addAll(addProductButton);
		layoutMenu.setStyle("-fx-background-color: DAE6F3;");

		layoutProducts = new VBox();
		layoutProducts.setTranslateX(-30);
		layoutProducts.setPadding(new Insets(50, 0, 0, 0));
		layoutProducts.setSpacing(4);

		layoutMenu.getChildren().add(layoutProducts);
	}
	
	@Override
	protected void initButtons() {
		addProductButton = new Button("ADD PRODUCT");
		addProductButton.setOnAction(e -> addProduct());
	}

	private void addProduct() {
		AddProductWindow addProductWindow = new AddProductWindow(this);
		addProductWindow.showAddProductWindow();

		product = new ImgBuyView(addProductWindow,this,user);
		
		layoutProducts.getChildren().add(product);
	}

	@Override
	protected void initLabels() {
	}
	
	/*
	 * 
	 * GETTERS AND SETTERS
	 * 
	 */
	
	public VBox getLayoutProducts() {
		return layoutProducts;
	}

}
