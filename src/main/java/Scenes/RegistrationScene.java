package Scenes;

import java.util.ArrayList;

import Database.MainDatabase;
import Database.entities.User;
import Main.Main;
import Validation.Validation;
import Window.AlertWindow;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class RegistrationScene extends AbstractScene {

	private GridPane layout;
	private Button registerButton;
	private Button backButton;
	private Label titleLabel;
	private Label nameLabel;
	private Label surnameLabel;
	private Label emailLabel;
	private Label passwordLabel;
	private Label addressLabel;
	private TextField nameTextField;
	private TextField surnameTextField;
	private TextField emailTextField;
	private TextField addressTextField;
	private PasswordField passPasswordField;

	private User user;

	public RegistrationScene(Stage window, Main main, double width, double height) {
		super(window, main, height, height);

		init(width, height);

		window.setScene(scene);
	}

	private void init(double width, double height) {
		initButtons();
		initLabels();
		initPasswordField();
		initTextField();
		initLayout();
		initScene(width, height);
	}

	@Override
	protected void initScene(double width, double height) {
		scene = new Scene(layout, width, height);
	}

	@Override
	protected void initLayout() {
		layout = new GridPane();
		layout.setPadding(new Insets(130, 80, 200, 50));
		layout.setVgap(15);
		layout.setHgap(10);
		layout.setStyle("-fx-background-color: DAE6F3;");
		layout.getChildren().addAll(titleLabel, nameLabel, surnameLabel, addressLabel, emailLabel, passwordLabel,
				passPasswordField, emailTextField, nameTextField, surnameTextField, addressTextField, registerButton,
				backButton);
	}

	protected void initButtons() {
		registerButton = new Button("   OK   ");
		registerButton.setTranslateX(120);
		registerButton.setTranslateY(200);
		registerButton.setOnAction(e -> {

			if (validateData() == true) {
				loadData();
				window.setScene(main.getStartScene().getScene());
			}
			else {
				new AlertWindow();
			}
		});

		backButton = new Button("back");
		backButton.setTranslateX(200);
		backButton.setTranslateY(230);
		backButton.setOnAction(e -> window.setScene(main.getStartScene().getScene()));
	}

	private boolean validateData() {
		boolean correct = false;

		if (Validation.NameValidator(nameTextField.getText()) && Validation.SurnameValidator(surnameTextField.getText())
				&& Validation.EmailValidator(emailTextField.getText())
				&& Validation.PasswordValidator(passPasswordField.getText()))
			correct = true;

		colorTextField();
		
		return correct;
	}

	private void colorTextField() {
		if(Validation.NameValidator(nameTextField.getText()) == false)
		{
			nameTextField.setStyle("-fx-background-color: #ed7765;");
		}
		else
		{
			nameTextField.setStyle("-fx-background-color: #83ed65;");
		}
		if(Validation.SurnameValidator(surnameTextField.getText())== false)
		{
			surnameTextField.setStyle("-fx-background-color: #ed7765;");
		}
		else
		{
			surnameTextField.setStyle("-fx-background-color: #83ed65;");
		}
		if(Validation.EmailValidator(emailTextField.getText())== false)
		{
			emailTextField.setStyle("-fx-background-color: #ed7765;");
		}
		else
		{
			emailTextField.setStyle("-fx-background-color: #83ed65;");
		}
		
		if(Validation.PasswordValidator(passPasswordField.getText()) == false)
		{
			passPasswordField.setStyle("-fx-background-color: #ed7765;");
		}
		else
		{
			passPasswordField.setStyle("-fx-background-color: #83ed65;");
		}
		
		// nie sprawdzam poprawnosci adresu
		addressTextField.setStyle("-fx-background-color: #83ed65;");
	}

	private void loadData() {

		user = new User();
		user.setAddress(addressTextField.getText());
		user.setEmail(emailTextField.getText());
		user.setName(nameTextField.getText());
		user.setSurname(surnameTextField.getText());
		user.setPassword(passPasswordField.getText());
		user.setTransactions(new ArrayList<>());
		
	
		MainDatabase.add(user);
	}

	private void initPasswordField() {
		passPasswordField = new PasswordField();
		passPasswordField.setText("password");
		GridPane.setConstraints(passPasswordField, 1, 4);
	}

	private void initTextField() {
		nameTextField = new TextField("name");
		GridPane.setConstraints(nameTextField, 1, 0);

		surnameTextField = new TextField("surname");
		GridPane.setConstraints(surnameTextField, 1, 1);

		addressTextField = new TextField("address 123, city, coutry");
		GridPane.setConstraints(addressTextField, 1, 2);

		emailTextField = new TextField("example@ex.com");
		GridPane.setConstraints(emailTextField, 1, 3);
	}

	protected void initLabels() {
		titleLabel = new Label("REGISTRATION");
		titleLabel.setTranslateY(-80);
		titleLabel.setTranslateX(110);

		nameLabel = new Label("name:");
		GridPane.setConstraints(nameLabel, 0, 0);

		surnameLabel = new Label("surname:");
		GridPane.setConstraints(surnameLabel, 0, 1);

		addressLabel = new Label("address:");
		GridPane.setConstraints(addressLabel, 0, 2);

		emailLabel = new Label("email:");
		GridPane.setConstraints(emailLabel, 0, 3);

		passwordLabel = new Label("password:");
		GridPane.setConstraints(passwordLabel, 0, 4);
	}
	
	/*
	 * 
	 * 
	 * GETTERS AND SETTERS
	 * 
	 */
	
	public User getUser() {
		return user;
	}
}
