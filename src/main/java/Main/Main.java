package Main;

import Database.MainDatabase;
import Scenes.StartScene;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static final double WIDTH = 400;
	public static final double HEIGHT = 400;

	private Stage window;
	private MainDatabase mainDatabase;
	private StartScene startScene;

	public void start(Stage arg0) throws Exception {
		window = arg0;
		
		startScene = new StartScene(window, this, WIDTH, HEIGHT);
		mainDatabase = new MainDatabase();

		window.setTitle("SKLEP");
		window.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	/*
	 * 
	 * GETTERS AND SETTERS
	 * 
	 * 
	 */

	public MainDatabase getMainDatabase() {
		return mainDatabase;
	}

	public void setMainDatabase(MainDatabase mainDatabase) {
		this.mainDatabase = mainDatabase;
	}

	public StartScene getStartScene() {
		return startScene;
	}

	public void setStartScene(StartScene startScene) {
		this.startScene = startScene;
	}
}
