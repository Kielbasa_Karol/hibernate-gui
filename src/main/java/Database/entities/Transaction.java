package Database.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Transaction {
	
	@Id
	@GeneratedValue
	private long id;
	private String test;
	
	@OneToMany
	@JoinColumn(name = "idtransaction")
	private List<OrderDetail> orderDetails;
	
	//TODO add date, and way of tramsport and GETTERS AND SETTRRS
	
	/*
	 * 
	 * GETTERS AND SETTERS
	 * 
	 * 
	 */
	
	public String getTest() {
		return test;
	}
	public void setTest(String test) {
		this.test = test;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}
}
