package Database;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.hibernate.Session;

import Database.entities.Transaction;
import Database.entities.User;

public class MainDatabase {

	static EntityManagerFactory entityManegerFactory;
	static EntityManager entityManager;

	public MainDatabase() {
		entityManegerFactory = Persistence.createEntityManagerFactory("myDatabase");
		entityManager = entityManegerFactory.createEntityManager();

	}

	public static void add(Object o) {
		entityManager.getTransaction().begin();
		entityManager.persist(o);
		entityManager.getTransaction().commit();
	}

	public static void remove(Object o) {
		entityManager.getTransaction().begin();
		entityManager.remove(o);
		entityManager.getTransaction().commit();
	}
	

	public static boolean find(String password, String email) {
		boolean found = false;

		TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u", User.class);
		List<User> users = query.getResultList();

		for (User user : users) {
			if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
				found = true;
			}
		}
		return found;
	}
	public static User loadUser(String password, String email) {
		User foundUser = null;

		TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u", User.class);
		List<User> users = query.getResultList();

		for (User user : users) {
			if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
				foundUser = user;
			}
		}
		return foundUser;
	}
}
