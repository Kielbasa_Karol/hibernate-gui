package Window;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertWindow {

	private Stage window;
	private Label textLabel;
	private Button closeButton;
	private VBox layout;
	private Scene scene;
	
	public AlertWindow()
	{
		window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle("ERROR");
		window.setResizable(false);

		initLabels();
		initButton();
		initTextFields();
		initLayout();
		initScene();
	}

	private void initLabels() {
		textLabel = new Label("Something gone wrong");
		textLabel.setTranslateX(40);
		textLabel.setTranslateY(20);
		
	}

	private void initButton() {
		closeButton = new Button("CLOSE");
		closeButton.setTranslateX(75);
		closeButton.setTranslateY(50);
		closeButton.setOnAction(e -> window.close());
	}

	private void initTextFields() {
		// TODO Auto-generated method stub
		
	}

	private void initLayout() {
		layout = new VBox();
		layout.setPadding(new Insets(10, 10, 10, 10));
		layout.setStyle("-fx-background-color: DAE6F3;");
		layout.getChildren().addAll(textLabel,closeButton);
		
	}

	private void initScene() {
		scene = new Scene(layout, 210, 130);
		window.setScene(scene);
		window.show();
		
	}
}
