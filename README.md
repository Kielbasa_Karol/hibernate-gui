**## Descripton in ENGLISH ##** : https://www.dropbox.com/s/mhswu65cj3a1ai5/Databse%20desc.pdf?dl=0

Repozytorium przedstawia prosta aplikacje sklepu z baza danych.                                                                                
Baza danych zawiera 4 tabele. Aby korzystać ze sklepu użytkownik musi się zarejestrowac a, następnie zalogować.                                               
System logowania i rejestracji posiada walidację danych.                                                                             

Aplikacja została napisana w jezyku *Java*. Do bazy danych użyto *JPA i Hibernate*.                                              
Projekt zostal zbudowany w oparciu o *Maven*. 

Przy pisaniu aplikacji korzystałem z video tutorial:                                                                   
* - **Artur Owczarek** https://www.youtube.com/playlist?list=PLU2dl_1LV_SQWZI2R_RSEeYm1tfueszOc                                                               
* - **thenewboston** https://www.youtube.com/playlist?list=PL6gx4Cwl9DGBzfXLWLSYVy8EbTdpGbUIG

Podsumowując, moim głównym celem napisania tej aplikacji było udowodnienie sobie że, potrafię „podpiąć” do projektu oraz zaprogramować relacyjną bazę danych. Mniejszą uwagę poświęcałem interfejsowi graficznemu oraz funkcjonalności tej aplikacji.


Link do PDFa z szerszym opisem jak działa baza danych:
https://www.dropbox.com/s/x6oze6ln02u0q4m/Opis%20bazy.pdf?dl=0